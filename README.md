# MockMW

A toolkit for building Morrowind mod plugins via CI.

This repository can be used to build mock plugins for various proprietary content for the game Elder Scrolls III: Morrowind.

#### Rationale

The tool [DeltaPlugin]() can compile plugins (`.esp`, `.omwaddon`, etc) from YAML files but it wants:

1. To read `$HOME/.config/openmw/openmw.cfg` for data paths and content files
1. To find actual plugins for any masters that are defined in the given YAML file

Unfortunately, many mods (rightfully) list the proprietary game data files for Morrowind and possibly other potentially proprietary data files made by modders.

MockMW aims to provide blank files that look like their proprietary counterparts but contain no actual data records aside from basic headers. This will allow using DeltaPlugin to produce plugins in a CI environment.

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Corsair83 for making [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364)
* Demanufacturer87 for making [Kogoruhn - Extinct City of Ash and Sulfur](https://www.nexusmods.com/morrowind/mods/51615)
* Hemaris for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)
* mort, Pozzo, Karpik777, and bhl for their work on [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) ([2.0](https://www.nexusmods.com/morrowind/mods/48225))
* RandomPal for making [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603)
* Reizeron for making [Redaynia Restored](https://www.nexusmods.com/morrowind/mods/47646)
* Resdayn Revival Team for making [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001)
* Stuporstar for making [Uvirith's Legacy](https://stuporstar.sarahdimento.com/)
* SVNR for making [Imperial Towns Revamp](https://www.nexusmods.com/morrowind/mods/49735)
* Tapetenklaus for making [Dr_Data](https://www.nexusmods.com/morrowind/mods/51776)
* wazabear for making [Ghastly Glowyfence](https://www.nexusmods.com/morrowind/mods/47982)
* The Tamriel Rebuilt Team for making [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/)
* The TOTSP Team for making [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Using In Your Project

The followind is an example of how you might use MockMW to build your own project:

```sh
# NOTE: replace the 1 tag with whatever the latest version is
docker run --rm -v $(pwd):/src registry.gitlab.com/modding-openmw/mockmw:1 ./build.sh
```

In the example above, `./build.sh` would be a small script that calls `delta_plugin` to build your project from YAML.

##### Projects Using MockMW

* [Abandoned Flat](https://gitlab.com/modding-openmw/abandoned-flat)
* [Abandoned Flat Containers](https://gitlab.com/modding-openmw/abandoned-flat-containers)
* [An Nwahs Guide To Modern Culture And Mythology](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology)
* [Distant Fixes Rise of House Telvanni](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni)
* [Distant Fixes Uviriths Legacy](https://gitlab.com/modding-openmw/distant-fixes-uviriths-legacy)
* [Distant Fixes Uviriths Manor](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor)
* [NCGDMW-Lua](https://gitlab.com/modding-openmw/ncgdmw-lua/-/blob/3bc0eceba36b05f4609d10668784854dc6bba26a/.gitlab-ci.yml#L7)
* [Total Overhaul Patches](https://gitlab.com/modding-openmw/total-overhaul-patches/-/blob/70e83b8cad76f93e353c3236dee7751bd1111d3e/.gitlab-ci.yml#L7)

#### Developing MockMW

The process of adding a new mock plugin to MockMW is something like this:

1. Copy the plugin you want to mock into the repo:

        cp /path/to/SomePlugin.esp .
1. Dump the plugin to YAML with DeltaPlugin:

        delta_plugin convert SomePlugin.esp
1. Get just the header data:

		# Note: If a plugin has scripts it will live in a `.d` subdir.
        # Also note: it may be more or less lines than 14 depending on the plugin!
        mv SomePlugin.yaml _SomePlugin.yaml 
        head -14 _SomePlugin.yaml > SomePlugin.yaml 
        rm _SomePlugin.yaml
1. Fix the `records` data: with your favorite text editor, edit the YAML file you created above and change `records:` to `records: {}`.
1. Add the new plugin to the `build.sh` script, check existing content for an idea of how it should be added based on its name.
1. Add a `Makefile` target for the new plugin.
1. Add the plugin to the `Dockerfile` so that it gets published as part of the whole package. Check existing content for an idea of how it should be added.

That should be it!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* Email: `johnnyhostile at modding-openmw dot com`
