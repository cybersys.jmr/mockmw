FROM alpine:3.15.4 as setup
ENV DELTA_PLUGIN_VERSION=0.18.0
ENV DELTA_PLUGIN_SHA=f1c04e79b788b2e6dc20d2abe0f0ca9b03c1f9e0f08c335505592b5fadc650fa
RUN apk add zip
RUN wget https://gitlab.com/bmwinger/delta-plugin/-/releases/$DELTA_PLUGIN_VERSION/downloads/delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip
RUN echo "$DELTA_PLUGIN_SHA  delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip" | sha256sum -c -
RUN unzip delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip
COPY *.yaml build.sh /
RUN PATH=.:$PATH ./build.sh

FROM alpine:3.15.4
COPY --from=setup [ \
    "/Morrowind.esm", \
    "/Tribunal.esm", \
    "/Bloodmoon.esm", \
    "/Tamriel_Data.esm", \
    "/TR_Mainland.esm", \
    "/OAAB_Data.esm", \
    "/Dr_Data.esm", \
    "/Rise of House Telvanni.esm", \
    "/Solstheim Tomb of The Snow Prince.esm", \
    "/StarwindRemasteredV1.15.esm", \
    "/StarwindRemasteredPatch.esm", \
    "/Abandoned_Flat.esp", \
    "/Beautiful cities of Morrowind.ESP", \
    "/HM_DDD_Strongholds_T_v1.0.esp", \
    "/Justice4Khartag+OAAB_Shipwreck - merged.ESP", \
    "/Kogoruhn - Extinct City of Ash and Sulfur.esp", \
    "/Nordic Dagon Fel.ESP", \
    "/OAAB - Shipwrecks.ESP", \
    "/OAAB Brother Junipers Twin Lamps.esp", \
    "/Redaynia Restored.ESP", \
    "/Riharradroon - Path to Kogoruhn v1.0.ESP", \
    "/RR_Better_Ships_n_Boats_Eng.ESP", \
    "/RR_Better_Ships_n_Boats_Eng.esp", \
    "/UL_3.5_RoHT_1.52_Add-on.esp", \
    "/UL_3.5_TR_21.01_Add-on.esp", \
    "/UL_3.5_TR_21.01_Add-on.omwaddon", \
    "/Uvirith\\'s Legacy_3.53.esp", \
    "/Uvirith\\'s Manor.ESP", \
    "/ghastly gg.esp", \
    "/srv/" \
]
COPY --from=setup /delta_plugin /usr/bin/
RUN mkdir -p "$HOME"/.config/openmw && echo "data=\"/srv\"" > "$HOME"/.config/openmw/openmw.cfg
WORKDIR /src
