#!/bin/sh
set -eu

target=${1-none}

mkdir -p tmp/.config/openmw
echo "data=\"$(pwd)\"" > tmp/.config/openmw/openmw.cfg

if [ "$target" = none ]; then
    # Build 'em all
    for m in Morrowind Tribunal Bloodmoon Tamriel_Data OAAB_Data TR_Mainland Dr_Data "Rise of House Telvanni"; do
        HOME=tmp delta_plugin convert "$m".yaml
        mv "$m".omwaddon "$m".esm
    done

    HOME=tmp delta_plugin convert "Beautiful cities of Morrowind.yaml"
    mv "Beautiful cities of Morrowind.omwaddon" "Beautiful cities of Morrowind.ESP"

    HOME=tmp delta_plugin convert "Redaynia Restored.yaml"
    mv "Redaynia Restored.omwaddon" "Redaynia Restored.ESP"

    HOME=tmp delta_plugin convert "Riharradroon - Path to Kogoruhn v1.0.yaml"
    mv "Riharradroon - Path to Kogoruhn v1.0.omwaddon" "Riharradroon - Path to Kogoruhn v1.0.ESP"

    HOME=tmp delta_plugin convert "Kogoruhn - Extinct City of Ash and Sulfur.yaml"
    mv "Kogoruhn - Extinct City of Ash and Sulfur.omwaddon" "Kogoruhn - Extinct City of Ash and Sulfur.esp"

    HOME=tmp delta_plugin convert UL_3.5_RoHT_1.52_Add-on.yaml
    mv UL_3.5_RoHT_1.52_Add-on.omwaddon UL_3.5_RoHT_1.52_Add-on.esp

    HOME=tmp delta_plugin convert UL_3.5_TR_21.01_Add-on.yaml
    cp UL_3.5_TR_21.01_Add-on.omwaddon UL_3.5_TR_21.01_Add-on.esp

    HOME=tmp delta_plugin convert "ghastly gg.yaml"
    mv "ghastly gg.omwaddon" "ghastly gg.esp"

    HOME=tmp delta_plugin convert RR_Better_Ships_n_Boats_Eng.yaml
    mv RR_Better_Ships_n_Boats_Eng.omwaddon RR_Better_Ships_n_Boats_Eng.ESP
    cp RR_Better_Ships_n_Boats_Eng.ESP RR_Better_Ships_n_Boats_Eng.esp

    HOME=tmp delta_plugin convert "OAAB - Shipwrecks.yaml"
    mv "OAAB - Shipwrecks.omwaddon" "OAAB - Shipwrecks.ESP"

    HOME=tmp delta_plugin convert "Nordic Dagon Fel.yaml"
    mv "Nordic Dagon Fel.omwaddon" "Nordic Dagon Fel.ESP"

    HOME=tmp delta_plugin convert "Nordic Dagon Fel.yaml"
    mv "Nordic Dagon Fel.omwaddon" "Nordic Dagon Fel.ESP"

    HOME=tmp delta_plugin convert StarwindRemasteredV1.15.yaml
    mv StarwindRemasteredV1.15.omwaddon StarwindRemasteredV1.15.esm

    HOME=tmp delta_plugin convert StarwindRemasteredPatch.yaml
    mv StarwindRemasteredPatch.omwaddon StarwindRemasteredPatch.esm

    HOME=tmp delta_plugin convert "Justice4Khartag+OAAB_Shipwreck - merged.yaml"
    mv "Justice4Khartag+OAAB_Shipwreck - merged.omwaddon" "Justice4Khartag+OAAB_Shipwreck - merged.ESP"

    HOME=tmp delta_plugin convert "Uvirith's Manor.yaml"
    mv "Uvirith's Manor.omwaddon" "Uvirith's Manor.ESP"

    HOME=tmp delta_plugin convert HM_DDD_Strongholds_T_v1.0.yaml
    mv HM_DDD_Strongholds_T_v1.0.omwaddon HM_DDD_Strongholds_T_v1.0.esp

    HOME=tmp delta_plugin convert "Uvirith's Legacy_3.53.yaml"
    mv "Uvirith's Legacy_3.53.omwaddon" "Uvirith's Legacy_3.53.esp"

    HOME=tmp delta_plugin convert Abandoned_Flat.yaml
    mv Abandoned_Flat.omwaddon Abandoned_Flat.esp

    HOME=tmp delta_plugin convert "Solstheim Tomb of The Snow Prince.yaml"
    mv "Solstheim Tomb of The Snow Prince.omwaddon" "Solstheim Tomb of The Snow Prince.esm"

    HOME=tmp delta_plugin convert "OAAB Brother Junipers Twin Lamps.yaml"
    mv "OAAB Brother Junipers Twin Lamps.omwaddon" "OAAB Brother Junipers Twin Lamps.esp"

else
    # Build a specific thing
    if [ "$target" = "Beautiful cities of Morrowind" ] \
           || [ "$target" = "Nordic Dagon Fel" ] \
           || [ "$target" = "OAAB - Shipwrecks" ] \
           || [ "$target" = "RR_Better_Ships_n_Boats_Eng" ] \
           || [ "$target" = "Redaynia Restored" ] \
           || [ "$target" = "Uvirith's Manor" ] \
           || [ "$target" = "Justice4Khartag+OAAB_Shipwreck - merged" ] \
           || [ "$target" = "Riharradroon - Path to Kogoruhn v1.0" ]; then
        HOME=tmp delta_plugin convert "$target".yaml
        mv "$target".omwaddon "$target".ESP

    elif [ "$target" = "StarwindRemasteredV1.15" ] \
        || [ "$target" = "StarwindRemasteredPatch" ] \
        || [ "$target" = "Solstheim Tomb of The Snow Prince" ]; then
        HOME=tmp delta_plugin convert "$target".yaml
        mv "$target".omwaddon "$target".esm

    elif [ "$target" = "Kogoruhn - Extinct City of Ash and Sulfur" ] \
         || [ "$target" = "Abandoned_Flat" ] \
         || [ "$target" = "OAAB Brother Junipers Twin Lamps" ] \
         || [ "$target" = "HM_DDD_Strongholds_T_v1.0" ] \
         || [ "$target" = "Uvirith's Legacy_3.53" ] \
         || [ "$target" = "UL_3.5_RoHT_1.52_Add-on" ] \
         || [ "$target" = "UL_3.5_TR_21.01_Add-on" ]; then
        HOME=tmp delta_plugin convert "$target".yaml
        mv "$target".omwaddon "$target".esp

    elif [ "$target" = UL_3.5_TR_21.01_Add-on ]; then
        HOME=tmp delta_plugin convert "$target".yaml

    else
        HOME=tmp delta_plugin convert "$target".yaml
        mv "$target".omwaddon "$target".esm
    fi
fi
