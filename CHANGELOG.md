## An N'wah's Guide To Modern Culture And Mythology Changelog

#### 1.3

* Properly implemented the audio via `Say` (special thanks to Settyness for this!)

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14927493) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.2

* Cleaned up audio tracks a bit (special thanks to Settyness for this!)

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14911282) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.1

* Re-recorded the audio tracks at a slightly higher level and with some cleanup.
* The odd NPC now has a secondary greeting after he's already greeted you and gave the book
* Removed the "psst" since it was too easy to overlap with the actual greeting.
* Actually included the audio files
* Fixed a bug where the player could receive multiple copies of the book.

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14877270) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14848545) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)
